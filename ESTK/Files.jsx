function readFileContent(fileObj, encoding) {
	var fileContent;

	encoding = encoding || "utf-8";
	fileObj = (fileObj instanceof File) ? fileObj : new File(fileObj);

	fileObj.open("r");
	fileObj.encoding = encoding;
	fileContent = fileObj.read();
	fileObj.close();

	return fileContent;
}

function writeFile(fileObj, fileContent, encoding) {
	encoding = encoding || "utf-8";
	fileObj = (fileObj instanceof File) ? fileObj : new File(fileObj);

	var parentFolder = fileObj.parent;
	if (!parentFolder.exists && !parentFolder.create())
		throw new Error("Cannot create file in path " + fileObj.fsName);

	fileObj.encoding = encoding;
	fileObj.open("w");
	fileObj.write(fileContent);
	fileObj.close();

	return fileObj;
}

function getFlatFileList(path) {
	var pathFiles = Folder(path).getFiles();
	var fileList = [];
	var subfiles;
	// Loop through the current folder's files and subfolders
	for (var i = 0, il = pathFiles.length; i < il; i++)
		if (pathFiles[i] instanceof Folder) {
			subfiles = getFlatFileList(pathFiles[i]);
			for (var j = 0, jl = subfiles.length; j < jl; j++)
				fileList[fileList.length] = subfiles[j];
		} else {
			// Add only files that end in .js or .jsx or .jsxbin, and that isn't this file itself
			if (pathFiles[i].name.match(/\.(js|jsx|jsxbin)$/)) {
				fileList.push(pathFiles[i].fsName);
			}
		}
	return fileList;
}

function getFileNamesFromArray(filesArray) {
	var namesArray = [],
		file;

	for (var i = 0, il = filesArray.length; i < il; i++) {
		file = filesArray[i];
		if (!(file instanceof File))
			file = File(file);

		namesArray.push(file.displayName.split(".")[0]);
	}

	return namesArray;
}

function getAllFiles(pathToFolder, recursion, extensionList) {
	var pathFiles = Folder(pathToFolder).getFiles(),
		files = [],
		subfiles;

	if (extensionList === undefined)
		extensionList = "";
	else if (typeof extensionList !== "function")
		extensionList = new RegExp("\.\(" + extensionList.replace(/,/g, "|").replace(/ /g, "") + ")$", "i");

	for (var i = 0, il = pathFiles.length; i < il; i++) {
		if (pathFiles[i] instanceof File) {
			if (pathFiles[i].name.match(extensionList)) {
				files.push(pathFiles[i]);
			}
		} else if (pathFiles[i] instanceof Folder && recursion === true) {
			subfiles = getAllFiles(pathFiles[i], recursion, extensionList);
			for (var j = 0, jl = subfiles.length; j < jl; j++) {
				files.push(subfiles[j]);
			}
		}
	}
	return files;
}

function selectFiles(multiSelect, extensionList) {
	var infoMessage = multiSelect ? "Please select multiple files" : "Please select file";
	var filter = ($.os.indexOf("Windows") != -1) ? "*." + extensionList.replace(/ /g, "").replace(/,/g, ";*.") : function(file) {
		var re = new RegExp("\.\(" + extensionList.replace(/,/g, "|").replace(/ /g, "") + ")$", "i");
		if (file.name.match(re) || file.constructor.name === "Folder")
			return true;
	};
	return File.openDialog(infoMessage, filter, multiSelect);
}

function mkdir(path) {
	var folder = new Folder(path);

	if (!folder.exists) {
		var parts = path.split('/');
		parts.pop();
		mkdir(parts.join('/'));
		folder.create();
	}
}

function findFileOrFolder(searchFolder, recursion, name, instanceType) {
	var item;
	var folderItems = searchFolder.getFiles();

	for (var i = 0, il = folderItems.length; i < il; i++) {
		item = folderItems[i];
		if (File.decode(item.name) === name && item instanceof instanceType)
			return item;
		else if (recursion === true && item instanceof Folder) {
			var item = findFileOrFolder(item, recursion, name, instanceType);
			if (item !== null) return item;
		}
	}
	return null;
}

function incrementFile(pathToFile) {
	var saveSufixLength = 2;
	var suffix = zeroPad(0, saveSufixLength);
	var newPathToFile = pathToFile;

	while (File(newPathToFile).exists) {
		var suffixInteger = parseInt(suffix, 10);
		suffixInteger += 1;
		suffix = zeroPad(suffixInteger, saveSufixLength);
		var folder = File(pathToFile).path;
		var fileName = File(pathToFile).name.substring(0, File(pathToFile).name.lastIndexOf("."));
		var extension = File(pathToFile).name.substring(File(pathToFile).name.lastIndexOf(".") + 1, File(pathToFile).name.length);

		newPathToFile = folder + "/" + fileName + "_" + suffix + "." + extension;
	}
	return newPathToFile;

	function zeroPad(num, digit) {
		var tmp = num.toString();
		while (tmp.length < digit) {
			tmp = "0" + tmp;
		}
		return tmp;
	}
}