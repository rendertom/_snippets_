/**********************************************************************************************
    Compare Files.jsx
    Copyright (c) 2017 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
		Compare the context of multiple files.
		Slow, but usefull when need to check for duplicate files in folder that have different names.
**********************************************************************************************/

doIt();
function doIt() {

	var sourceFiles = File.openDialog("Select files", undefined, {
		multiSelect: true
	});

	if (!sourceFiles) return;
	var curFile, compareFile,
		curFileText, compareFileText;

	for (var i = 0, il = sourceFiles.length; i < il; i ++) {
		curFile = sourceFiles[i];
		curFileText = readFileContent(curFile);
		for (var j = 0, jl = sourceFiles.length; j < jl; j ++) {
			compareFile = sourceFiles[j];
			if (curFile !== compareFile) {
				compareFileText = readFileContent(compareFile);
				if (curFileText === compareFileText) {
					alert(curFile.displayName + " === " + compareFile.displayName);
				}
			}
		}
	}
	alert("done");
}

function readFileContent(fileObj, encoding) {
	encoding = encoding || "utf-8";

	fileObj.open('r');
	fileObj.encoding = encoding;
	var fileContent = fileObj.read();
	fileObj.close();
	return fileContent;
}