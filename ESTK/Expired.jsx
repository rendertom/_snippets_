/**********************************************************************************************
    Expired.jsx
    Copyright (c) 2016 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
		Simple way to set scripts expiration date.
		Pass date to function expired() in format YYYYMMDD.
		Returns TRUE if expired, else FALSE.
**********************************************************************************************/

var scriptExpired = expired(20100925);

if (scriptExpired)
	alert("You cannot run this script anymore because you haven't paid the developer.", "Error", true);

function expired(expirationDate) {
	var date = new Date(),
		yy = date.getFullYear(),
		mm = date.getMonth() + 1,
		dd = date.getDate(),
		today;

	mm = (mm < 10) ? "0" + mm : mm;
	dd = (dd < 10) ? "0" + dd : dd;

	today = yy.toString() + mm.toString() + dd.toString();

	expirationDate = (typeof expirationDate !== "string") ? expirationDate.toString() : expirationDate;

	// alert("Today: " + today + ", Expires: " + expirationDate)

	return (today > expirationDate) ? true : false;
}