var ProgressBar = function (panelTitle) {
	"use strict";

	var win = new Window("palette", panelTitle, undefined);
	win.alignChildren = ["fill", "top"];
	win.preferredSize.width = 360;
	win.spacing = 10;

	win.etProgressName = win.add("statictext");
	win.pbProgressBar = win.add("progressbar");
	win.pbProgressBar.preferredSize.height = 5;
	win.etProgressInfo = win.add("statictext");

	this.name = function (progressName) {
		win.etProgressName.text = progressName;
		// this.info("");
	};

	this.info = function (progressInfo) {
		win.etProgressInfo.text = progressInfo;
	};

	this.show = this.reset = function (progressName, maxValue) {
		win.pbProgressBar.value = 0;
		win.pbProgressBar.maxvalue = maxValue;

		this.name(progressName);

		win.show();
	};

	this.up = function (progressInfo) {
		win.pbProgressBar.value++;

		this.info(progressInfo);
		win.update();
	};

	this.hide = function () {
		win.hide();
	};

	this.close = function () {
		win.close();
	};
};



//------------------------------------------------  
//      SAMPLE CODE  
//------------------------------------------------  

// (function () {
// 	var PB = new ProgressBar("Script Title"),
// 		vMax;

// 	// Quick process  
// 	// ---  
// 	vMax = 50;
// 	PB.show("Doing Somehing", vMax);
// 	for (var i = 0; i < vMax; i++) {
// 		PB.up("Counting Sheeps: " + (i + 1));
// 	}


// 	PB.name("Wait for 800 ms...");
// 	$.sleep(800);

// 	// Slow process  
// 	// ---  
// 	vMax = 13;
// 	PB.show("And now slowly", vMax);
// 	for (i = 0; i < vMax; i++) {
// 		$.sleep(100);
// 		PB.up((100 * (i + 1) / vMax).toFixed(2) + "%");
// 	}

// 	$.sleep(500);
// 	PB.name("The job is done. Great success!");
// 	$.sleep(1500);

// 	// Quit  
// 	// ---  
// 	PB.close();
// })();