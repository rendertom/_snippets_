(function () {
	try {
		var doc = app.activeDocument;

		var items = findLayers(doc, true, {
			typename : "ArtLayer",
			kind: LayerKind.SMARTOBJECT,
		});

		alert(items)

	} catch (e) {
		alert(e.toString() + "\nScript File: " + File.decode(e.fileName).replace(/^.*[\|\/]/, '') +
			"\nFunction: " + arguments.callee.name +
			"\nError on Line: " + e.line.toString());
	}

	function findLayers(searchFolder, recursion, userData, items) {
		items = items || [];
		var folderItem;
		for (var i = 0, il = searchFolder.layers.length; i < il; i++) {
			folderItem = searchFolder.layers[i];
			if (propertiesMatch(folderItem, userData)) {
				items.push(folderItem);
			}
			if (recursion === true && folderItem.typename === "LayerSet") {
				findLayers(folderItem, recursion, userData, items);
			}
		}
		return items;
	}

	function propertiesMatch(projectItem, userData) {
		if (typeof userData === "undefined") return true;
		for (var propertyName in userData) {
			if (!userData.hasOwnProperty(propertyName)) continue;
			if (!projectItem.hasOwnProperty(propertyName)) return false;
			if (projectItem[propertyName].toString() !== userData[propertyName].toString()) {
				return false;
			}
		}
		return true;
	}
})();