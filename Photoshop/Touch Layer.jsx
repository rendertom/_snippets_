var doc = app.activeDocument;
var myLayer = touchLayer(doc.layers[4]);

alert(myLayer.name);

function touchLayer(layer) {
	app.activeDocument.activeLayer = layer;

	var desc = new ActionDescriptor();
	var ref = new ActionReference();
	ref.putIdentifier(app.charIDToTypeID('Lyr '), layer.id);
	desc.putReference(app.charIDToTypeID('null'), ref);
	executeAction(app.charIDToTypeID('slct'), desc, DialogModes.NO);

	return layer;
}

function touchLayer(layer, appendToSelection) {
	app.activeDocument.activeLayer = layer;

	var ref = new ActionReference(),
		desc = new ActionDescriptor(),
		s2t = function (s) {
			return stringIDToTypeID(s);
		};

	ref.putIdentifier(s2t("layer"), layer.id);
	desc.putReference(s2t("null"), ref);
	if (appendToSelection)
		desc.putEnumerated(
			s2t("selectionModifier"),
			s2t("selectionModifierType"),
			s2t("addToSelection")
		);
	desc.putBoolean(s2t("makeVisible"), false);
	executeAction(s2t("select"), desc, DialogModes.NO);

	return layer;
}