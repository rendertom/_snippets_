/**********************************************************************************************
    Set Random Text Color.jsx
    Copyright (c) 2016 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
		Script applies random color from given
		color array	to all text layers in the document.
**********************************************************************************************/

(function () {

	// Define HEX colors separated by comma
	var colorArray = ["#000000", "#f3f3f3", "#76f8f7", "#333333", "#454566", "#d87d56"];

	// Collect all text layers in the document
	var textLayers = getTextLayers(app.activeDocument);

	// Loop through all text layers in the document
	for (var t = 0, tl = textLayers.length; t < tl; t++) {
		// Get random color from color array
		var randomColor = colorArray[Math.floor(Math.random() * colorArray.length)];
		// Remove pound sing from the color value
		if (randomColor.charAt(0) === "#")
			randomColor = randomColor.substr(1);

		// Initiate new color
		var textColor = new SolidColor();
		textColor.rgb.hexValue = randomColor;

		// Set text Layer Color
		textLayers[t].textItem.color = textColor;
	}


	function getTextLayers(doc, layers) {
		layers = layers || [];
		for (var i = 0, il = doc.layers.length; i < il; i++) {
			if (doc.layers[i].typename == "LayerSet") {
				getTextLayers(doc.layers[i], layers);
			} else if (doc.layers[i].kind == "LayerKind.TEXT") {
				layers.push(doc.layers[i]);
			}
		}
		return layers;
	}

})();