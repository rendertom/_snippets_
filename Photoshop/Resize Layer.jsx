/**********************************************************************************************
    Resize Layer.jsx
    Copyright (c) 2017 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
		Resizes layer to given dimentions
**********************************************************************************************/

function resizeLayer(layer, dimensions, upscale) {
	app.preferences.rulerUnits !== Units.PIXELS && app.preferences.rulerUnits = Units.PIXELS;		
	app.activeDocument.mode !== ChangeMode.RGB && app.activeDocument.changeMode(ChangeMode.RGB);
	layer.isBackgroundLayer === true && layer.isBackgroundLayer = false;

	var canvas = {};
	if (dimensions === undefined) {
		canvas.width = app.activeDocument.width;
		canvas.height = app.activeDocument.height;
	} else {
		if (dimensions.constructor === Array && dimensions.length === 2) {
			canvas.width = dimensions[0];
			canvas.height = dimensions[1];
		} else if ((dimensions.constructor === Object || dimensions.constructor === Document) &&
			dimensions.hasOwnProperty("width") && dimensions.hasOwnProperty("height")) {
			canvas.width = dimensions.width;
			canvas.height = dimensions.height;
		} else {
			throw new Error ("Function \"" + arguments.callee.name + "\" takes \"dimensions\" argument either as two dimensional array or object with \"Width\" and \"Height\" properties.");
		}
	}

	var layerWidth  = Number(layer.bounds[2] - layer.bounds[0]);
	var layerHeight = Number(layer.bounds[3] - layer.bounds[1]);

	var percent = (canvas.width / layerWidth > canvas.height / layerHeight)
					? 100 * canvas.width / layerWidth
					: 100 * canvas.height / layerHeight;

	if (percent > 100 && upscale !== undefined && upscale === false)
		return;

	layer.resize(percent, percent, AnchorPosition.MIDDLECENTER); 
}