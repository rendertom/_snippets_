/**********************************************************************************************
    Center in Container.jsx
    Copyright (c) 2017 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
		Centers layer in a given container.
		If container is not defined, then centers in the canvas.
**********************************************************************************************/

var doc = app.activeDocument;
var layer = doc.layers[0];
var container = doc.layers[1];

centerInContainer(layer, container);

function centerInContainer(layer, container) {
	var targetBounds = [0, 0, 0, 0],
		targetCenter = {},
		sourceCenter = {},
		getElementCenter = function(bounds) {
			return {
				x: Number(bounds[2] - bounds[0]) / 2 + Number(bounds[0]),
				y: Number(bounds[3] - bounds[1]) / 2 + Number(bounds[1])
			};
		};

	if (container === undefined || container.constructor === Document) {
		targetBounds[2] = app.activeDocument.width;
		targetBounds[3] = app.activeDocument.height;
	} else if (container.hasOwnProperty("bounds")) {
		targetBounds = container.bounds;
	} else if (container.constructor === Array) {
		if (container.length === 2) {
			targetBounds[2] = container[0];
			targetBounds[3] = container[1];
		} else if (container.length === 4) {
			targetBounds = container;
		}
	} else {
		throw new Error("Function \"" + arguments.callee.name + "\" takes \"container\" argument either as 2 or 4 dimensional array or object with 4 dimensional bounds property.");
	}

	targetCenter = getElementCenter(targetBounds);
	sourceCenter = getElementCenter(layer.bounds);

	layer.translate(targetCenter.x - sourceCenter.x, targetCenter.y - sourceCenter.y);
}