/**********************************************************************************************
    AE Object to Source.jsx
    Copyright (c) 2017 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
		Similar to JSON.stringify, this snippet should stringify AE's item or layer.

	Note:
		It's still buggy and unfinished.
**********************************************************************************************/

(function () {
	try {

		var itemToProcess = getItemToProcess();
		if (!itemToProcess) return;

		var result = objectToSource(itemToProcess);

		writeFile(Folder.desktop.fsName + "/obj to source.js", result);

	} catch (e) {
		alert(e.toString() + "\nScript File: " + File.decode(e.fileName).replace(/^.*[\|\/]/, '') +
			"\nFunction: " + arguments.callee.name +
			"\nError on Line: " + e.line.toString());
	}

	function objectToSource(object) {
		if (typeof object === "object") {
			if (!objectToSource.check) objectToSource.check = [];
			for (var i = 0, k = objectToSource.check.length; i < k; ++i) {
				if (objectToSource.check[i] == object) {
					return "{}";
				}
			}
			objectToSource.check.push(object);
		}

		var key = "",
			str = "";

		for (var property in object) {
			if (property.match("maxValue") && object.hasMax === false)
				continue;
			if (property.match("minValue") && object.hasMin === false)
				continue;
			if (property.match("lightType") && object.layer !== LightLayer)
				continue;
			if (property.match("separationDimension") && object.dimensionsSeparated === false)
				continue;
			if (property.match("separationLeader") && object.dimensionsSeparated === false)
				continue;
			if (!object[property])
				continue;
			if (!object.hasOwnProperty(property))
				continue;
			if (typeof object[property] === "function")
				continue;

			if (!isArray(object)) key = "\"" + property + "\":";

			if (typeof object[property] === "string") {
				str += key + "\"" + object[property] + "\",";
			} else if (isArray(object[property])) {
				str += key + object[property].toSource() + ",";
			} else if (typeof object[property] === "object") {
				str += key + objectToSource(object[property]) + ",";
			} else {
				str += key + object[property] + ",";
			}
		}

		if (typeof object == "object") objectToSource.check.pop();

		str = isArray(object) ? "[" + str.slice(0, -1) + "]" : "{" + str.slice(0, -1) + "}";

		return str;
	}

	function writeFile(pathToFile, content, encoding) {
		encoding = encoding || "utf-8";

		var newFile = new File(pathToFile);
		newFile.encoding = encoding;
		newFile.open("w");
		newFile.write(content);
		newFile.close();
	}

	function isArray(object) {
		return Object.prototype.toString.apply(object) === "[object Array]";
	}

	function getItemToProcess() {
		var itemToProcess = app.project.selection[0];
		if (!itemToProcess)
			return alert ("Please select one project item");

		if (itemToProcess instanceof CompItem) {
			if (itemToProcess.selectedLayers.length > 0) {
				itemToProcess = itemToProcess.selectedLayers[0];
			}
		}

		return itemToProcess;
	}

})();