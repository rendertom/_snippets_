// Get Composition Presets from Preference file.

(function() {

	/* jshint ignore:start */
	if(typeof JSON!=="object"){JSON={}}(function(){function f(n){return n<10?"0"+n:n}function this_value(){return this.valueOf()}if(typeof Date.prototype.toJSON!=="function"){Date.prototype.toJSON=function(){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+f(this.getUTCMonth()+1)+"-"+f(this.getUTCDate())+"T"+f(this.getUTCHours())+":"+f(this.getUTCMinutes())+":"+f(this.getUTCSeconds())+"Z":null};Boolean.prototype.toJSON=this_value;Number.prototype.toJSON=this_value;String.prototype.toJSON=this_value}var cx,escapable,gap,indent,meta,rep;function quote(string){escapable.lastIndex=0;return escapable.test(string)?'"'+string.replace(escapable,function(a){var c=meta[a];return typeof c==="string"?c:"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+string+'"'}function str(key,holder){var i,k,v,length,mind=gap,partial,value=holder[key];if(value&&typeof value==="object"&&typeof value.toJSON==="function"){value=value.toJSON(key)}if(typeof rep==="function"){value=rep.call(holder,key,value)}switch(typeof value){case"string":return quote(value);case"number":return isFinite(value)?String(value):"null";case"boolean":case"null":return String(value);case"object":if(!value){return"null"}gap+=indent;partial=[];if(Object.prototype.toString.apply(value)==="[object Array]"){length=value.length;for(i=0;i<length;i+=1){partial[i]=str(i,value)||"null"}v=partial.length===0?"[]":gap?"[\n"+gap+partial.join(",\n"+gap)+"\n"+mind+"]":"["+partial.join(",")+"]";gap=mind;return v}if(rep&&typeof rep==="object"){length=rep.length;for(i=0;i<length;i+=1){if(typeof rep[i]==="string"){k=rep[i];v=str(k,value);if(v){partial.push(quote(k)+(gap?": ":":")+v)}}}}else{for(k in value){if(Object.prototype.hasOwnProperty.call(value,k)){v=str(k,value);if(v){partial.push(quote(k)+(gap?": ":":")+v)}}}}v=partial.length===0?"{}":gap?"{\n"+gap+partial.join(",\n"+gap)+"\n"+mind+"}":"{"+partial.join(",")+"}";gap=mind;return v}}if(typeof JSON.stringify!=="function"){escapable=/[\\\"\u0000-\u001f\u007f-\u009f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;meta={"\b":"\\b","\t":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"};JSON.stringify=function(value,replacer,space){var i;gap="";indent="";if(typeof space==="number"){for(i=0;i<space;i+=1){indent+=" "}}else{if(typeof space==="string"){indent=space}}rep=replacer;if(replacer&&typeof replacer!=="function"&&(typeof replacer!=="object"||typeof replacer.length!=="number")){throw new Error("JSON.stringify")}return str("",{"":value})}}if(typeof JSON.parse!=="function"){cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;JSON.parse=function(text,reviver){var j;function walk(holder,key){var k,v,value=holder[key];if(value&&typeof value==="object"){for(k in value){if(Object.prototype.hasOwnProperty.call(value,k)){v=walk(value,k);if(v!==undefined){value[k]=v}else{delete value[k]}}}}return reviver.call(holder,key,value)}text=String(text);cx.lastIndex=0;if(cx.test(text)){text=text.replace(cx,function(a){return"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)})}if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,""))){j=eval("("+text+")");return typeof reviver==="function"?walk({"":j},""):j}throw new SyntaxError("JSON.parse")}}}());
	/* jshint ignore:end */


	// Preference files tend to change from version to version.
	// This particular method works on CC2017 and CC2018.
	// Older AE versions have different preferences, so
	// this method will not work on older versions.
	
	if (parseFloat(app.version) < 14 /* CC2017 */) {
		return alert("This method works for AE higher then CC2017");
	}

	try {
		
		// If you try reading prefecences like this:
		// app.preferences.getPrefAsString(
		// 		"Composition Presets Section v10",
		// 		"001",
		// 		PREFType.PREF_Type_MACHINE_INDEPENDENT_COMPOSITION
		// );
		// you will get "Error: After Effects error: could not convert Unicode characters.".
		// 
		// The only workaround for this is to read prefs file as string and then parse it.

		var prefsFile = getPrefsFile();
		var prefsFileContent = readFileContent(prefsFile);
		var presetsAsString = getTextBlockAsString(prefsFileContent);

		// Builds Presets object like this:
		// {
		//  	"000": "02D001E0001DF8\"R\"0000000A0000000B",
    	//		"001": "02D001E0001DF8\"R\"000000\"(\"000000\"!\"",
    	//		...
		// }
		var compositionPresets = buildCompositionPresetsFromString(presetsAsString);

		var presetObject = {};

		var presetNamesSection = "Composition Preset Names Section v10";
		var key = 0;
		var keyPadded = pad(key);

		while (havePref(presetNamesSection, keyPadded)) {
			var presetName = getPrefAsString(presetNamesSection, keyPadded);

			var compDataAsciiString = compositionPresets[keyPadded];
			var compDataObject = compositionPresetToObject(compDataAsciiString);

			presetObject[presetName] = compDataObject;

			keyPadded = pad(key++);
		}

		alert(JSON.stringify(presetObject, false, 4));

	} catch (e) {
		alert(e.toString());
	}

	function getPrefsFile() {
		var appVersion = parseFloat(app.version).toFixed(1);
		var isWindows = $.os.indexOf("Windows") !== -1;
		var isMac = !isWindows;

		var folderPath = Folder.userData.fsName + "/Adobe/After Effects/" + appVersion;
		if (isMac) {
			folderPath = Folder.userData.parent.fsName + "/Preferences/Adobe/After Effects/" + appVersion;
		}

		var prefsFile = folderPath + "/" + "Adobe After Effects " + appVersion + " Prefs-indep-composition.txt";

		return prefsFile
	}

	function readFileContent(fileObj, encoding) {
		var fileContent;

		encoding = encoding || "UTF-8";
		fileObj = (fileObj instanceof File) ? fileObj : new File(fileObj);

		if (!fileObj.exists) {
			throw "File " + fileObj.fsName + " doesn't exist!";
		}

		fileObj.open("r");
		fileContent = fileObj.read();
		fileObj.close();

		if (fileContent === null) {
			throw "Unable to read contents of " + fileObj.fsName;
		}

		return fileContent;
	}

	function getTextBlockAsString(prefsAsString) {
		var presetsSection = "Composition Presets Section v10";

		var start = prefsAsString.indexOf('["' + presetsSection + '"]');
		var end = prefsAsString.length;

		var textBlockAsString = prefsAsString.substring(start, end);
		return textBlockAsString;
	}

	function buildCompositionPresetsFromString(presetsAsString) {
		var compositionPresets, textLines, split, key, value, i, il;

		compositionPresets = {};
		textLines = presetsAsString.split(/\n|\r/);
		for (i = 1, il = textLines.length; i < il; i++) {
			if (textLines[i] === "") continue;

			split = textLines[i].split(" = ");
			key = split[0];
			key = trim(key);
			key = key.replace(/\"/g, "");
			value = split[1];

			compositionPresets[key] = value;
		}

		return compositionPresets;
	}


	function compositionPresetToObject(asciiString) {
		var hexString = asciiToHexString(asciiString);
		var chunks = splitToChunks(hexString, 4);

		var width = hexStringToDec(chunks[0]);
		var height = hexStringToDec(chunks[1]);
		var frameRate = hexStringToDec(chunks[2]);

		// chunks 3 to 7 have some values that I was not able to figure out,
		// so I am skipping them.
		
		return {
			width: width,
			height: height,
			frameRate: frameRate
		};
	}





	// Utilities
	
	function trim(string) {
		return string.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
	}

	function pad(number, length) {
		length = length || 3;

		var string = String(number);
		while (string.length < length) {
			string = "0" + string;
		}
		return string;
	}

	function splitToChunks(string, chunkSize) {
		var chunks = [];
		for (var i = 0, il = string.length; i < il; i += chunkSize) {
			chunks.push(string.substring(i, i + chunkSize));
		}

		return chunks;
	}

	function havePref(section, key) {
		if (typeof PREFType === "undefined") {
			return app.preferences.havePref(section, key);
		} else {
			return app.preferences.havePref(section, key, PREFType.PREF_Type_MACHINE_INDEPENDENT_COMPOSITION);
		}
	}

	function getPrefAsString(section, key) {
		if (typeof PREFType === "undefined") {
			return app.preferences.getPrefAsString(section, key);
		} else if (typeof PREFType !== "undefined") {
			return app.preferences.getPrefAsString(section, key, PREFType.PREF_Type_MACHINE_INDEPENDENT_COMPOSITION);
		}
	}

	function asciiToHexString(asciiString) {
		var hexString, hexCharacter, insideParathenses;

		hexString = "";
		insideParathenses = false;

		for (var i = 0, il = asciiString.length; i < il; i++) {
			if (asciiString[i] === "\"") {
				insideParathenses = !insideParathenses;
			} else {
				hexCharacter = asciiString[i];
				if (insideParathenses) {
					hexCharacter = hexCharacter.charCodeAt(0).toString(16);
				}
				hexString += hexCharacter;
			}
		}

		return hexString.toUpperCase();
	}

	function hexStringToDec(hexString) {
		var decimal = parseInt(hexString, 16);
		return decimal;
	}

})();