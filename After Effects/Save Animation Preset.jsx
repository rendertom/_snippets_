/**********************************************************************************************
    Save Animation Preset.jsx
    Copyright (c) 2016 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
    	Script saves Animation Preset (FFX file) and returns path to it.
**********************************************************************************************/

(function () {
	try {

		var myComp = app.project.activeItem;
		if (!myComp || !(myComp instanceof CompItem))
			return alert("Select a comp");

		var myLayer = myComp.selectedLayers[0];
		if (!myLayer)
			return alert("Select a layer");

		var myProps = myLayer.selectedProperties;
		if (myProps.length === 0)
			return alert("Select some properties for fun.");

		var animationPreset = saveAnimationPreset();
		alert(animationPreset);

	} catch (e) {
		alert(e.toString() + "\nScript File: " + File.decode(e.fileName).replace(/^.*[\|\/]/, '') +
			"\nFunction: " + arguments.callee.name +
			"\nError on Line: " + e.line.toString());
	}


	/**
	 * [getLastSavedAnimationPreset Gets latest used animation preset from Animation -> Recent Animation Presets list ]
	 * @return {string / null} [Returns file path to recently used preset / null if there are no recent presets]
	 */
	function getLastSavedAnimationPreset() {
		if (app.preferences.havePref("Most Recently Used (MRU) Search v2", "MRU Favorite Effects Path ID # 0, File Path")) {
			var lastPreset = app.preferences.getPrefAsString("Most Recently Used (MRU) Search v2", "MRU Favorite Effects Path ID # 0, File Path");
			if (File(lastPreset).exists)
				return lastPreset;
		}
		return null;
	}

	/**
	 * [saveAnimationPreset Calls "Save Animation Preset" menu command and saves Animation Preset]
	 * @return {string / null} [Returns file path to saved file / null if user canceled.]
	 */
	function saveAnimationPreset() {
		var lastSavedAnimationPreset = getLastSavedAnimationPreset();

		var oldTime, newTime;

		if (lastSavedAnimationPreset !== null)
			oldTime = File(lastSavedAnimationPreset).modified.getTime();

		app.executeCommand(3075); // Save Animation Preset;

		var newAnimationPreset = getLastSavedAnimationPreset();

		if (newAnimationPreset === null)
			return null;

		if (lastSavedAnimationPreset === null && newAnimationPreset !== null)
			return newAnimationPreset;

		newTime = File(newAnimationPreset).modified.getTime();

		if (oldTime === newTime) {
			return null;
		} else if (oldTime < newTime) {
			return newAnimationPreset;
		}

		alert("Something went wrong in determining if user saved the Animation Preset");
		return null;
	}

})();