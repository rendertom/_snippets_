/**********************************************************************************************
    Create Text Layer.jsx
    Copyright (c) 2017 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
		Script checks if desired font is installed on system;
		Also checks if read-only values match desired combination.
		If not - user is prmpted to set UI values for himself.

	TODO:
		Clean-up the code.
**********************************************************************************************/

doIt();

function doIt() {

	var fontStyles = {
		font: "Bellico",
		fontURL : "http://www.1001freefonts.com/bellico.font",
		fauxBold : false,
		fauxItalic : false,
		allCaps : false,
		smallCaps : false,
		superscript : false,
		subscript : false
	};

	if (!checkFontOptions(fontStyles)) return;

	app.beginUndoGroup("Create Text Layer");

	var myComp = app.project.items.addComp("Text Composition", 1920, 1080, 1, 10, 24);
		myComp.time = 0;
		myComp.bgColor = [0.12549019607843,0.64705882352941,0.97254901960784];
		myComp.openInViewer();

	var textLayer 	= myComp.layers.addText("you're good to go!");
	var textProp 	= textLayer.property("ADBE Text Properties").property("ADBE Text Document");
	var textValue 	= textProp.value;

		textValue.font 			= fontStyles.font;
		textValue.fontSize 		= 200;
		textValue.applyFill 	= true;
		textValue.fillColor 	= [0.99215686321259,0.74901962280273,0.15686275064945];
		textValue.applyStroke 	= false;
		textValue.justification = ParagraphJustification.CENTER_JUSTIFY;
		textValue.tracking 		= 0;
		if (parseFloat(app.version) >= 13.2 ) {
			textValue.verticalScale 	= 1;
			textValue.horizontalScale 	= 1;
			textValue.baselineShift 	= 0;
			textValue.tsume 			= 0;
			// These values are read-only. This is why we are running test above.
			// textValue.fauxBold = fontStyles.fauxBold;
			// textValue.fauxItalic = fontStyles.fauxItalic;
			// textValue.allCaps = fontStyles.allCaps;
			// textValue.smallCaps = fontStyles.smallCaps;
			// textValue.superscript = fontStyles.superscript;
			// textValue.subscript = fontStyles.subscript;
		}
		textProp.setValue(textValue);
		textLayer.property("ADBE Transform Group").property("ADBE Anchor Point").setValue([6.52619934082031,-44.4257850646973,0]);

	app.endUndoGroup();
}

function checkFontOptions(fontStyles) {
	var myComp 		= app.project.items.addComp("temp comp", 100, 100, 1, 1, 25);
	var textLayer 	= myComp.layers.addText("temp text");
	var textProp 	= textLayer.property("ADBE Text Properties").property("ADBE Text Document");
	var textValue 	= textProp.value;
		textValue.font = fontStyles.font;
		textProp.setValue(textValue);

	var	newTextValue = textProp.value;

	myComp.remove();

	var errorArr = [];
	for (var propertyName in fontStyles) {
		if (!fontStyles.hasOwnProperty(propertyName)) continue;
		
		if (fontStyles[propertyName] !== newTextValue[propertyName]) {
			if (propertyName === "font") {
				alert("You don't have \"" + fontStyles[propertyName] + "\" font installed on your system.\nYou can get this font from " + fontStyles.fontURL);
				return false;
			} else if (parseFloat(app.version) >= 13.2) {
				errorArr.push(propertyName + " : " +  fontStyles[propertyName] );
			}
		}
	}

	if (errorArr.length > 0) {
		var errorAlert = "Alert! These font style options are read only. You will have to set them yourself for script to continue working:\n";
		var badProperties = errorArr.toString().replace(/,/g, "\n");
		alert(errorAlert + badProperties);
		return false;
	} else {
		return true;
	}
}