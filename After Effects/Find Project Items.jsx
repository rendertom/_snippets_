/**********************************************************************************************
    Find Project Items.jsx
    Copyright (c) 2017 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
		Searches for Items in Project panel.
		Accepts userData object as options to match search criteria.
**********************************************************************************************/

(function () {
	try {

		var items = findProjectItems(app.project.rootFolder, true, {
			name: "Dark Gray Solid 1",
			mainSource: {
				loop: 1,
				alphaMode: 5412,
				color: [0.34303003549576, 0.34303003549576, 0.34303003549576]
			},
			comment: "",
			parentFolder: {
				parentFolder: {
					comment: "ppp"
				}
			}
		});

		alert(items);

	} catch (e) {
		alert(e.toString() + "\nScript File: " + File.decode(e.fileName).replace(/^.*[\|\/]/, '') +
			"\nFunction: " + arguments.callee.name +
			"\nError on Line: " + e.line.toString());
	}


	/**
	 * Searches for Items in Project Panel.
	 * @param  FolderItem 	searchFolder - folder where to start looking for the item;
	 * @param  Boolean 		recursion - True to search in nested folders.
	 * @param  Object 		userData - Object with additional properties, such as { name: "name", label: 0, comment: "", typeName:"Composition" }
	 * @return Object     	Returns array of items that matches given criteria
	 */
	function findProjectItems(searchFolder, recursion, userData, items) {
		items = items || [];
		var folderItem;
		for (var i = 1, il = searchFolder.items.length; i <= il; i++) {
			folderItem = searchFolder.items[i];
			if (propertiesMatch(folderItem, userData)) {
				items.push(folderItem);
			} else if (recursion === true && folderItem instanceof FolderItem && folderItem.numItems > 0) {
				findProjectItems(folderItem, recursion, userData, items);
			}
		}
		return items;
	}

	function propertiesMatch(projectItem, userData) {
		if (typeof userData === "undefined") return true;

		for (var propertyName in userData) {
			if (!userData.hasOwnProperty(propertyName)) continue;

			if (typeof userData[propertyName] !== typeof projectItem[propertyName])
				return false;

			if (isArray(userData[propertyName]) && isArray(projectItem[propertyName])) {
				if (userData[propertyName].toString() !== projectItem[propertyName].toString()) {
					return false;
				}
			} else if (typeof userData[propertyName] === "object" && typeof projectItem[propertyName] === "object") {
				if (!propertiesMatch(projectItem[propertyName], userData[propertyName])) {
					return false;
				}
			} else if (projectItem[propertyName] !== userData[propertyName]) {
				return false;
			}
		}
		return true;
	}

	function isArray(object) {
		return Object.prototype.toString.apply(object) === "[object Array]";
	}
})();