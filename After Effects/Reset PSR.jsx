/**********************************************************************************************
    Reset PSR.jsx
    Copyright (c) 2017 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
		Resets layers Transform property to default value.
**********************************************************************************************/


(function () {
	resetPSR();

	function resetPSR() {
		var composition = app.project.activeItem;
		if (!composition || !(composition instanceof CompItem))
			return alert("Please select composition first");

		app.beginUndoGroup("Reset PSR");
		app.executeCommand(2605); // Transform -> Reset
		app.endUndoGroup();
	}
})();