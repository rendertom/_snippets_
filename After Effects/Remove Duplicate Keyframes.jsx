/**********************************************************************************************
    Remove duplicate keyframes.jsx
    Copyright (c) 2017 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
		Script removes duplicate keyframes from selected compositions.
**********************************************************************************************/

(function () {
	try {
		var comp, layer, prop;
		var properties = [];
		var y, numRemoved = 0;
		var curVal, nextVal;

		app.beginUndoGroup("Remove Duplicate Keyframes");
		
		clearOutput();
		for (var r = 0, rl = app.project.selection.length; r < rl; r++) {
			comp = app.project.selection[r];
			if (comp instanceof CompItem) {
				for (var s = 1, sl = comp.numLayers; s <= sl; s++) {
					layer = comp.layer(s);
					properties = getPropsWithKeys(layer);

					for (var a = 0, al = properties.length; a < al; a++) {
						prop = properties[a];
						if (prop.numKeys > 1) {
							writeLn(r + ":" + a + " - " + comp.name + ": " + prop.name);
							y = 1;
							while (y < prop.numKeys) {

								curVal = prop.keyValue(y);
								nextVal = prop.keyValue(y + 1);

								if (prop.matchName === "ADBE Mask Shape" ||
									prop.matchName === "ADBE Vector Shape") {
									curVal = curVal.vertices;
									nextVal = nextVal.vertices;
								}

								if (curVal.toString() === nextVal.toString()) { // &&
									// (prop.keyInInterpolationType(y) === KeyframeInterpolationType.HOLD) && 
									// (prop.keyOutInterpolationType(y) === KeyframeInterpolationType.HOLD) &&
									// (prop.keyInInterpolationType(y+1) === KeyframeInterpolationType.HOLD) && 
									// (prop.keyOutInterpolationType(y+1) === KeyframeInterpolationType.HOLD)) {

									prop.removeKey(y + 1);
									numRemoved++;
								} else {
									y++;
								}
							}
						}
					}
				}
			}
		}

		app.endUndoGroup();

		alert("Removed " + numRemoved + " duplicate keyframes");

	} catch (e) {
		alert(e.toString() + "\nScript File: " + e.fileName.replace(/^.*[\\|\/]/, '') +
			"\nFunction: " + arguments.callee.name +
			"\nError on Line: " + e.line.toString());
	}

	function getPropsWithKeys(currentProperty, propsArray) {
		propsArray = propsArray || [];
		for (var i = 1, il = currentProperty.numProperties; i <= il; i++) {
			if (currentProperty.property(i).numKeys > 0) {
				propsArray.push(currentProperty.property(i));
			}
			getPropsWithKeys(currentProperty.property(i), propsArray);
		}
		return propsArray;
	}
})();