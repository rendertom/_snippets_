/**********************************************************************************************
    Get Modified Properties.jsx
    Copyright (c) 2017 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
		Returns an array of modified properties:
		layer -> propertyGroup -> propertyGroup -> ... -> modifiedProperty
**********************************************************************************************/

var myLayer = app.project.activeItem.selectedLayers[0];
var modifiedProperties = getModifiedProperties(myLayer);

alert(modifiedProperties.join(",\n"));

function getModifiedProperties(property, propertiesArray) {
	propertiesArray = propertiesArray || [];
	var curentProperty, propertyChain;

	for (var i = 1; i <= property.numProperties; i++) {
		curentProperty = property.property(i);
		if (curentProperty.isModified && curentProperty.propertyType === PropertyType.PROPERTY) {
			propertyChain = getPropertyChain(curentProperty);
			propertiesArray.push(propertyChain);
		}
		getModifiedProperties(curentProperty, propertiesArray);
	}
	return propertiesArray;
}

function getPropertyChain(property) {
	var propertyChain = [];
	while (property) {
		propertyChain.unshift(property); // set this to propertyChain.unshift(prop.name) to see the names of the properties
		property = property.parentProperty;
	}
	return propertyChain;
}