/**********************************************************************************************
    Get properties with Keyframes.jsx
    Copyright (c) 2017 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
		Loops through selected layer and returns properties that have keyframes
**********************************************************************************************/
(function () {
	try {

		var myComp = app.project.activeItem;
		if (!myComp || !(myComp instanceof CompItem))
			return alert("Please select composition first");

		var myLayer = myComp.selectedLayers[0];
		if (!myLayer)
			return alert("Select layer first.");

		var propsWithKeys = getPropsWithKeys(myLayer);
		alert(propsWithKeys);

	} catch (e) {
		alert(e.toString() + "\nScript File: " + File.decode(e.fileName).replace(/^.*[\|\/]/, '') +
			"\nFunction: " + arguments.callee.name +
			"\nError on Line: " + e.line.toString());
	}

	function getPropsWithKeys(currentProperty, propsArray) {
		propsArray = propsArray || [];
		for (var i = 1; i <= currentProperty.numProperties; i++) {
			if (currentProperty.property(i).numKeys > 0) {
				propsArray.push(currentProperty.property(i));
			}
			getPropsWithKeys(currentProperty.property(i), propsArray);
		}
		return propsArray;
	}
})();