/**********************************************************************************************
    UI: Stroke over Button.jsx
    Copyright (c) 2016 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
		Example of how to draw stroke over an image button.

	Note: onDraw event is buggy and will probably crash your script.
**********************************************************************************************/

demo();

function demo() {

	var myDefs = {
		buttonSize 			: [85, 85],
		strokeSize 			: 10,
		strokeColorOver 	: [0.5, 1, 1],
		strokeColorActive	: [0.5, 0.5, 1]
	};

	var image = "\u0089PNG\r\n\x1A\n\x00\x00\x00\rIHDR\x00\x00\x00U\x00\x00\x00U\b\x03\x00\x00\x00\x0F+Xh\x00\x00\x00\u00E4PLTE'''\u00FF\u00FF\u00FF\u00FFZ\x00\u00EC\u00EC\u00EC\u00E6\u00E6\u00E6MMM+++\u00F6\u00F6\u00F6\u00D9\u00D9\u00D9aaa\u00A7\u00A7\u00A7\u00F4\u00F4\u00F4\u00EDU\x03\u00DB\u00DB\u00DB}}}@??zzz,(&)'&\u00FC\u00FC\u00FC\u00EE\u00EE\u00EE\u00D1\u00D1\u00D1\u00BB\u00BB\u00BBIII987///8+#\u00CCN\t\u00E8T\x04\u00DD\u00DD\u00DD\u00D5\u00D5\u00D5\u00C6\u00C6\u00C6\u00B0\u00B0\u00B0\u009F\u009F\u009F\u008A\u008A\u008Ayyyrrriii\\\\\\444J. T1\x1F\u0083<\x16\u00B5F\r\u00FCY\x00\u00E7\u00E7\u00E7\u00E1\u00E1\u00E1\u00CA\u00CA\u00CA\u008F\u008F\u008Fuuu^^^TTT_SM666A-\"n6\x1At9\x19\u009FC\x11\u00E3S\x05\u00EF\u00EF\u00EF\u00CD\u00CD\u00CD\u00A4\u00A4\u00A4\u009C\u009C\u009C\u0082\u0082\u0082\x7F\x7F\x7FRIEEEEa5\x1D\u0093A\x13\u009CC\x12\u00A9F\x0F\u00B6I\r\u00C4L\x0B\u00C7K\n\u00DEQ\x06\u00F6X\x02\u008B[\u009C\u00BE\x00\x00\x01{IDATX\u00C3\u00ED\u00D4\u00C7n\x021\x10\u0080\u00E11N(1\u00CB\u00B2\u0095\x1Az\u00EF\x10ZH\u00EF\u00ED\u00FD\u00DF'\x04{\u00B3H\u00AC\x12i='\u00E4\u00FF\u00E6\u00CB\u00A7\u00D1X6\u00A8T*\u0095J\u00A5:\u009E\u00D2\u00D4\u00A6\u00FB\u00C5\u00DD\u00C9\u00AC-\u00AD\u00EA%r\u00D0\u00B8\u0091\u0093eS\x169\u00CC\u00CDH\u00AA\u00B98\tH\u00BB\u0090d\u00A7$\u00A8\u00F3\u008C\u00E4\u00B0Z [nKn\u0096\x04V\u0093S\u00DB\x0E!\u00C3T=\u00BD\u00AD^\u009F\u0094\x7Fw\u00A0K\x0F[\x02\u00AF\u00C5\u009D%\u00D8\u0094\u009C\u009A\u00BD&\u00A3\u00A8\x7F\u00BC\x17j%*\u00C76\u009C}a!\u0096\u00E0d\x013q}C\x1DUmr\u00F5\nWmp\u0095\u00A2\u00AA\u00D1\nW5\u00D4\u00BD&b\\us\u0088hK\x0B\u00FF\u00B82\u00A7\u00C1\u00CD\u00AAT\u00A04\u00C4\u00B7\u00A5\u009D\x04g\x13\u00AFj\u0088G\u00C0\u0088\x1F\u00DEWH\u00FFAY\x13\u00F0U6\x05|5\u00DE\u0084P\u00D9\x7F\rz\u00F3\b\u00E1\u00A2\u00B1\u009F\x18\u00B38d\u00C5x\u008CQ\u00A7\u00A6C\u00D8Z\u0089]-\u0097\u00AB#~\u009C\u00CF\x1F\u00B2\x00F\u00DE\x04\u00B9\u00AA\\-\u0081\u0097\u00D1Y\u00F5\x0BKI\u00F5L\\Ot'\u00F6\u00FA\u00FD\u008F\u00CFA$\u00D2\u00C3T\u00CD^\u0084\u00B7Y\"\u00AA\u0090/\b\u00F6\x19S\u0085\u00F5\u0080\u00AB\u00C5<\u00A2\u00EA\x0F\u00FB\u0082\u00A9\u00C2\u00FAK\f\u00DB\u00C1TMo\u00D8KL\x15V\u00DEf\u00BB\u0098\u00AA\u00F9\u00EE\rk\"\u00AA\u00F0$\u00D4dWZ\u00D5|\u00D5x\x13\u00EC\u00AB\x11Z\u00BDet\u009B=\u00F6U\u00E8\x16\u0093\u00BB6\x1DP\u00A9T*\u0095Ju\x14}\x03\u00D2\u0082\x1C\u00B7Q\u00FD\u00B2\u008E\x00\x00\x00\x00IEND\u00AEB`\u0082";
	var imgArray = [image, image];

	var win = new Window("palette", "Demo", undefined, {resizeable:true});

	var btn;
	var doDraw = function () {
		// draw image
		this.graphics.drawImage(this.image, 0, 0, myDefs.buttonSize[0], myDefs.buttonSize[1]);

		// draw stroke
		var strokePen = this.graphics.newPen(this.graphics.PenType.SOLID_COLOR, this.strokeColorOver, this.strokeSize);
		this.graphics.rectPath(0, 0, myDefs.buttonSize[0], myDefs.buttonSize[1]);
		this.graphics.strokePath(strokePen);
	};
	var onMouseOver = function() {
		this.strokeSize = myDefs.strokeSize;
		this.strokeColorOver = myDefs.strokeColorOver;
		this.notify("onDraw");
	};
	var onMouseDown = function (event) {
		if (event.detail == 2) {
			this.strokeColorOver = myDefs.strokeColorActive;
			this.notify("onDraw");
		}
	};
	var onMouseOut = function () {
		this.strokeSize = null;
		this.notify("onDraw");
	};



	for (var i = 0; i < imgArray.length; i ++) {
		btn = win.add("button", undefined, imgArray[i]);
		btn.size = myDefs.buttonSize;	
		btn.image = ScriptUI.newImage(imgArray[i]);

		btn.onDraw = doDraw;
		btn.addEventListener("mouseover", onMouseOver);
		btn.addEventListener("mousedown", onMouseDown);
		btn.addEventListener("mouseout", onMouseOut);
	}

	win.onResizing = win.onResize = function () {this.layout.resize();};

	win.center();
	win.show();
}